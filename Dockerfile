FROM php:7.2-fpm
COPY app /var/www/html
CMD ["php","-S", "0.0.0.0:7171"]
EXPOSE 7171
